import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const localUrl = 'https://jsonplaceholder.typicode.com/posts';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

    sendFileName(url, fileName) {
      return this.http.post(url, fileName, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
    }

}
