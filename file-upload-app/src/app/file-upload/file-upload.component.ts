import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  uploadedFile: File = null;
  fileUploadForm: any;
  
  /**
   * 
   * @param api This is the service which helps to make the rest-api calls 
   * @param formBuilder It has the complete control over the form
   */
  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    this.fileUploadForm = this.formBuilder.group({
      fileupload: ['', Validators.required],
      
    });
   }

  /**
   * Incase if we need to execute anything as soon as this component loads
   * then that logic must be written in ngOnInit()
   */ 
  ngOnInit() {
  }

  /**
   * 
   * @param file When we click on the choose file button this method is called and
   * selected file will be available in this variable
   * 
   */
  upload(file: FileList) {
    console.log(file);
    this.uploadedFile = file.item(0);
    
  }

  /**
   * On clicking submit the below method is called.
   */
  onSubmit() {
    if( this.fileUploadForm.invalid ) {
      alert('No File Selected');
      return;
    }else {
      let data = {};
      let url = 'rest-api-call-url';
      data['fileName'] = this.uploadedFile['name'];

      /**
       * API call to the backend. Substitute the actual api call in the url. 
       */

      this.api.sendFileName(url, data)
      .subscribe(response => {
        alert('Operation Successful');
        this.fileUploadForm.reset();
      },(err) => {
        console.log('Error ',err);
        alert('Operation Failed');
      }) 
    }
  }

  get uploadFormControl(){
    return this.fileUploadForm.controls;
  }
}
